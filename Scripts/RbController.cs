﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody2D))]
public class RbController : MonoBehaviour
{
    [SerializeField] private float jumpForce = 400f;                                 
    [Range(0, .3f)] [SerializeField] private float movementSmoothing = .05f;  
    [SerializeField] private bool airControl = false;
    [SerializeField] private LayerMask whatIsGround;               
    [SerializeField] private Transform groundCheck;

    private SpriteRenderer sRenderer;
    const float groundedRadius = .2f; 
    private bool grounded;
    private bool inAir;
    private bool climbing = false;
    private Rigidbody2D rb2D;
    private Vector3 velocity = Vector3.zero;

    private void Awake()
    {
        sRenderer = GetComponent<SpriteRenderer>();
        rb2D = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        bool wasGrounded = grounded;
        grounded = false;

        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        // This can be done using layers instead but Sample Assets will not overwrite your project settings.
        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, groundedRadius, whatIsGround);
        Debug.DrawLine(groundCheck.position, Vector3.down * groundedRadius);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                grounded = true;
                inAir = false;
            }
        }
    }


    public void Move(Vector2 move, bool jump)
    {
        if ((grounded || climbing) && jump)
        {
            inAir = true;
            grounded = false;
            rb2D.AddForce(Vector2.up * jumpForce,ForceMode2D.Impulse);
        }

        if (climbing && !inAir)
        {
            Vector3 targetVelocity = move * 10f;
            rb2D.velocity = Vector3.SmoothDamp(rb2D.velocity, targetVelocity, ref velocity, movementSmoothing);
            sRenderer.flipX = (move.x > 0) ? true : ((move.x < 0) ? false : sRenderer.flipX);

            return;
        }

        if (grounded || airControl)
        {
            Vector3 targetVelocity = new Vector2(move.x * 10f, rb2D.velocity.y);
            rb2D.velocity = Vector3.SmoothDamp(rb2D.velocity, targetVelocity, ref velocity, movementSmoothing);

            sRenderer.flipX = (move.x > 0) ? true : ((move.x < 0) ? false : sRenderer.flipX);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Ladder"))
            climbing = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Ladder"))
            climbing = false;
    }

}
