﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    RbController rbControl;
    [SerializeField] float speed = 4f;
    float horizontalMovment;
    float verticalMovment;
    Vector2 movment;
    bool jump = false;
    [SerializeField] UIController uiController;

    void Start()
    {
        rbControl = GetComponent<RbController>();
    }

    void Update()
    {
        horizontalMovment = Input.GetAxisRaw("Horizontal") * speed;
        verticalMovment = Input.GetAxisRaw("Vertical") * speed;
        movment.x = horizontalMovment;
        movment.y = verticalMovment;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            jump = true;
        }
    }

    private void FixedUpdate()
    {
        rbControl.Move(movment*Time.fixedDeltaTime, jump);
        jump = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Barell"))
        {
            uiController.PauseGame();
            uiController.DisplayPanel(1);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("WinTrigger"))
        {
            uiController.PauseGame();
            uiController.DisplayPanel(0);
        }
    }

}
