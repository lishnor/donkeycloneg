﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrel : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("BarrelDestroyer"))
        {
            Destroy(gameObject, 0.1f);
        }
    }
}
