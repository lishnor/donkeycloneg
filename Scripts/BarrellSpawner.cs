﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrellSpawner : MonoBehaviour
{
    [SerializeField] GameObject barrelToSpawn;
    [SerializeField] float timeSpan;
    float currentTime;

    private void Start()
    {
        currentTime = Time.time;
    }

    void Update()
    {
        if (Time.time > currentTime)
        {
            currentTime += timeSpan;
            Instantiate(barrelToSpawn, transform.position, Quaternion.identity);
        }
    }
}
